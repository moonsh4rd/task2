import UIKit

class ViewController: UIViewController {

    private var game = Game()
    @IBOutlet private var cardButtons: [UIButton]!
    @IBOutlet private weak var deckCardCounter: UIButton!
    @IBOutlet private weak var newGameButton: UIButton!
    @IBOutlet private weak var scoreLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        newGame()
    }
    
    @IBAction func newGameButtonPressed(_ sender: UIButton) {
      newGame()
    }
    
    func highlightCard(_ cardNumber: Int, _ button: UIButton) {
        if game.gameDeck[cardNumber].isActivated {
            button.backgroundColor = UIColor.yellow
        } else {
            button.backgroundColor = UIColor.white
        }
    }
    
    func ruleChecker() {
        if game.cardsLeft >= 3 && game.cardsRevealed < 12 {
            dealCards()
        }
        for index in game.cardsRevealed-1..<cardButtons.count {
            let button = cardButtons[index]
            button.isHidden = true
        }
    }
    
    func newGame() {
        game.setDefaultValues()
        game.createDeck()
        game.shuffleTheDeck()
        updateViewFromModel()
    }
    
    func updateViewFromModel(){
        ruleChecker()
        for index in 0..<game.cardsRevealed {
            let button = cardButtons[index]
            let info = game.gameDeck[index].getInfo()
            let attributes = game.getFillAndColor(info)
            let resultingString = NSAttributedString(string: game.getString(info), attributes: attributes)
            button.setAttributedTitle(resultingString, for: .normal)
            highlightCard(index, button)
            button.isHidden = false
        }
        scoreLabel.text = "\(game.score)"
        deckCardCounter.setTitle("\(game.cardsLeft)", for: .normal)
    }
    
    @IBAction func touchCard(_ sender: UIButton) {
        if let cardNumber = cardButtons.index(of: sender){
            game.selectCard(cardNumber)
        } else {
            print("Card was not set in cardButtons")
        }
        updateViewFromModel()
    }
    
    @IBAction func dealCards() {
        if game.cardsRevealed+3<24, game.cardsLeft != 0 {
            for index in game.cardsRevealed-1..<game.cardsRevealed+3 {
                cardButtons[index].isHidden = false
            }
            game.cardsLeft -= 3
            game.cardsRevealed += 3
            updateViewFromModel()
        }
    }
}
