import Foundation

    enum CardSymbolType: Int {
        case char1 = 0
        case char2
        case char3
        
        static var allCases: [CardSymbolType]  {return [.char1, .char2, .char3]}
    }
    
    enum CardSymbolAmount: Int {
        case one = 0
        case two
        case three
        
        static var allCases: [CardSymbolAmount]  {return [.one, .two, .three]}
    }
    
    enum CardFillType: Int {
        case solid = 0
        case stripped
        case blank
        
        static var allCases: [CardFillType]  {return [.solid, .stripped, .blank]}
    }
    
    enum CardColorType: Int {
        case red = 0
        case green
        case blue
        
        static var allCases: [CardColorType]  {return [.red, .green, .blue]}
    }
    
    
    

