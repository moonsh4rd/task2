import Foundation

class Card {
    
    private var info: CardInfo
    var isActivated = false
    
    init(info: CardInfo) {
        self.info = info
    }
    
    func getInfo() -> CardInfo {
        return info
    }
}
