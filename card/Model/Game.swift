
import Foundation
import UIKit

class Game {
    private var baseDeck = [CardInfo]()
    private(set) var gameDeck = [Card]()
    var cardsLeft = Int()
    private(set) var score = Int()
    var cardsRevealed = Int()
    private var firstSelectedCard = Int()
    private var secondSelectedCard = Int()
    private var thirdSelectedCard = Int()
    private var activatedCardsCounter = Int()
    
    func setDefaultValues() {
        baseDeck = [CardInfo]()
        gameDeck = [Card]()
        cardsLeft = 69
        score = 0
        cardsRevealed = 12
        firstSelectedCard = -1
        secondSelectedCard = -1
        thirdSelectedCard = -1
        activatedCardsCounter = 0
    }
    
    func getSymbol(_ info: CardInfo) -> String {
        switch info.symbol {
        case .char1:
            return "▲"
        case .char2:
            return "●"
        case .char3:
            return "■"
        }
    }
    
    func getString(_ info: CardInfo) -> String {
        switch info.amount {
        case .one:
            return getSymbol(info)
        case .two:
            return getSymbol(info)+getSymbol(info)
        case .three:
            return getSymbol(info)+getSymbol(info)+getSymbol(info)
        }
    }
    
    func getColor(_ info: CardInfo) -> UIColor {
        switch info.color {
        case .red:
            return UIColor.red
        case .green:
            return UIColor.green
        case .blue:
            return UIColor.blue
        }
    }
    
    func getFillAndColor(_ info: CardInfo) -> [NSAttributedStringKey: Any] {
        let color = getColor(info)
        switch info.fill {
        case .solid:
            return [.foregroundColor : color, .font: UIFont.systemFont(ofSize: 15)]
        case .blank:
            return [.strokeColor: color, .foregroundColor: UIColor.white, .strokeWidth: -5.0, .font: UIFont.systemFont(ofSize: 15)]
        case .stripped:
            return [.foregroundColor: color.withAlphaComponent(0.15), .strokeWidth: -5.0, .font: UIFont.systemFont(ofSize: 15)]
        }
    }
    
    func checkTheSetExistence() {
        var setLogicalCounter = 0
        let firstInfo = gameDeck[firstSelectedCard].getInfo()
        let secondInfo = gameDeck[secondSelectedCard].getInfo()
        let thirdInfo = gameDeck[thirdSelectedCard].getInfo()
        if (firstInfo.amount == secondInfo.amount && firstInfo.amount == thirdInfo.amount) || (firstInfo.amount != secondInfo.amount && firstInfo.amount != thirdInfo.amount) {
            setLogicalCounter += 1
        }
        if (firstInfo.color == secondInfo.color && firstInfo.color == thirdInfo.color) || (firstInfo.color != secondInfo.color && firstInfo.color != thirdInfo.color) {
            setLogicalCounter += 1
        }
        if (firstInfo.fill == secondInfo.fill && firstInfo.fill == thirdInfo.fill) || (firstInfo.fill != secondInfo.fill && firstInfo.fill != thirdInfo.fill) {
            setLogicalCounter += 1
        }
        if (firstInfo.symbol == secondInfo.symbol && firstInfo.symbol == thirdInfo.symbol) || (firstInfo.symbol != secondInfo.symbol && firstInfo.symbol != thirdInfo.symbol) {
            setLogicalCounter += 1
        }
        if setLogicalCounter == 4 {
            gameDeck[firstSelectedCard].isActivated = false
            gameDeck[secondSelectedCard].isActivated = false
            gameDeck[thirdSelectedCard].isActivated = false
            gameDeck.remove(at: firstSelectedCard)
            gameDeck.remove(at: secondSelectedCard)
            gameDeck.remove(at: thirdSelectedCard)
            activatedCardsCounter = 0
            score += 4
            cardsRevealed -= 3
            firstSelectedCard = -1
            secondSelectedCard = -1
            thirdSelectedCard = -1
        } else {
            score -= 1
            gameDeck[firstSelectedCard].isActivated = false
            gameDeck[secondSelectedCard].isActivated = false
            gameDeck[thirdSelectedCard].isActivated = false
            activatedCardsCounter = 0
            firstSelectedCard = -1
            secondSelectedCard = -1
            thirdSelectedCard = -1
        }
    }
    
    func createDeck() {
        for symbol in 0...2 {
            for amount in 0...2 {
                for fill in 0...2 {
                    for color in 0...2{
                        baseDeck.append(CardInfo(symbol: CardSymbolType.allCases[symbol], amount: CardSymbolAmount.allCases[amount], fill: CardFillType.allCases[fill], color: CardColorType.allCases[color]))
                    }
                }
            }
        }
    }
    
    func shuffleTheDeck() {
        for _ in 0..<81 {
            let randomIndex = Int(arc4random_uniform(UInt32(baseDeck.count-1)))
            gameDeck.append(Card(info: baseDeck[randomIndex]))
            baseDeck.remove(at: randomIndex)
        }
    }
    
    func selectCard(_ cardNumber: Int) {
        if cardNumber != firstSelectedCard, cardNumber != secondSelectedCard, cardNumber != thirdSelectedCard, activatedCardsCounter == 3 {
            checkTheSetExistence()
        }
        if firstSelectedCard == -1 {
            firstSelectedCard = cardNumber
            gameDeck[firstSelectedCard].isActivated = true
            activatedCardsCounter += 1
        } else {
            if firstSelectedCard == cardNumber {
                gameDeck[firstSelectedCard].isActivated = false
                firstSelectedCard = -1
                activatedCardsCounter -= 1
            }
            if firstSelectedCard != -1, secondSelectedCard == -1 {
                secondSelectedCard = cardNumber
                gameDeck[secondSelectedCard].isActivated = true
                activatedCardsCounter += 1
            } else {
                if secondSelectedCard == cardNumber {
                    gameDeck[secondSelectedCard].isActivated = false
                    secondSelectedCard = -1
                    activatedCardsCounter -= 1
                }
                if firstSelectedCard != -1, secondSelectedCard != -1, thirdSelectedCard == -1 {
                    thirdSelectedCard = cardNumber
                    gameDeck[thirdSelectedCard].isActivated = true
                    activatedCardsCounter += 1
                } else {
                    if thirdSelectedCard == cardNumber {
                        gameDeck[thirdSelectedCard].isActivated = false
                        thirdSelectedCard = -1
                        activatedCardsCounter -= 1
                    }
                }
            }
        }
    }
}
