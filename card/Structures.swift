import Foundation

struct CardInfo {
    var symbol: CardSymbolType
    var amount: CardSymbolAmount
    var fill: CardFillType
    var color: CardColorType
}
